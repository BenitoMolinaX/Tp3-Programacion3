package xml;

import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import logica.DatosLocalidad;

public class GuardarXML {

public GuardarXML(ArrayList<DatosLocalidad> datos,String nombre_archivo, String directorio){
	
	if(datos.isEmpty()){
        JOptionPane.showMessageDialog(null, "No se cargaron datos");
        return;
    }else{

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        DOMImplementation implementation = builder.getDOMImplementation();
        Document document = implementation.createDocument(null, nombre_archivo, null);
        document.setXmlVersion("1.0");

        
        //Main Node
        Element raiz = document.getDocumentElement();
        //Por cada key creamos un item que contendrá la key y el value
        for(int i=0; i<datos.size();i++){
            //Item Node
            Element itemNode = document.createElement("dato"); 

            //Value Node
            Element valueNode = document.createElement("provincia"); 
            Text nodeValueValue = document.createTextNode(datos.get(i).getProv());                
            valueNode.appendChild(nodeValueValue);
            itemNode.appendChild(valueNode);
            
             valueNode = document.createElement("localidad"); 
             nodeValueValue = document.createTextNode(datos.get(i).getLocalidad());                
            valueNode.appendChild(nodeValueValue);
            itemNode.appendChild(valueNode);
            
            valueNode = document.createElement("habitantes"); 
            nodeValueValue = document.createTextNode(datos.get(i).getHab().toString());                
            valueNode.appendChild(nodeValueValue);
            itemNode.appendChild(valueNode);
            
            valueNode = document.createElement("long"); 
            nodeValueValue = document.createTextNode(datos.get(i).getLon().toString());                
            valueNode.appendChild(nodeValueValue);
            itemNode.appendChild(valueNode);
            
            valueNode = document.createElement("lat"); 
            nodeValueValue =  document.createTextNode(datos.get(i).getLat().toString());                
            valueNode.appendChild(nodeValueValue);
            itemNode.appendChild(valueNode);
            

            itemNode.appendChild(valueNode);
            //append itemNode to raiz
            raiz.appendChild(itemNode); //pegamos el elemento a la raiz "Documento"
        	
        }     
        
        
        //Generate XML
        Source source = new DOMSource(document);
        //Indicamos donde lo queremos almacenar
        Result result = new StreamResult(new java.io.File(directorio + "\\"+nombre_archivo+".xml")); //nombre del archivo
        Transformer transformer = null;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
        try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
    }
	
}
	
}
