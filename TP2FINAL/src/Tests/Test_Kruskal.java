package Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import logica.Costos;
import logica.DatosLocalidad;
import logica.Solver;

public class Test_Kruskal {
	

	@Test
	public void test() {
		Costos costo = new Costos(100.00, 200.00,400.00);
		ArrayList<DatosLocalidad>  conexiones = new ArrayList<DatosLocalidad>();
		cargarconexiones(conexiones);
		Solver solver = new Solver(conexiones,  costo);
			
		System.out.println("Costo Total =  $ " + solver.costoTotal());
	}
	@Test
	public void test2 (){
		Costos costo = new Costos(100.00, 200.00,400.00);
		ArrayList<DatosLocalidad>  conexiones = new ArrayList<DatosLocalidad>();
		
		cargarconexiones2(conexiones);
		Solver solver = new Solver(conexiones,  costo);
			System.out.println("Costo Total Test 2 = $ "+solver.costoTotal());

	}
	@Test
	public void testCiudades0 (){
		Costos costo = new Costos(100.00, 200.00,400.00);
		ArrayList<DatosLocalidad>  conexiones = new ArrayList<DatosLocalidad>();
		
		cargarconexiones3(conexiones);
		Solver solver = new Solver(conexiones,  costo);
			System.out.println("Costo Total Test ciudades0 = $ "+solver.costoTotal());
			Assert.assertEquals(0, 0 , solver.costoTotal());	
	}
	public void cargarconexiones3(ArrayList<DatosLocalidad> c){
		
	}
	@Test
	public void testCercaniaProvincia(){
		Costos costo = new Costos(100.00, 9000.00,100.00);
		ArrayList<DatosLocalidad>  conexiones = new ArrayList<DatosLocalidad>();
		ArrayList<DatosLocalidad>  conexiones2 = new ArrayList<DatosLocalidad>();
		cargarconexiones4(conexiones);
		cargarconexiones5(conexiones2);
		Solver solver = new Solver(conexiones,  costo);
		Solver solver2 = new Solver(conexiones2,  costo);
			System.out.println("Costo Total  misma provincia = $ "+solver.costoTotal());
			System.out.println("Costo Total  otra provincia = $ "+solver2.costoTotal());
			Assert.assertTrue(solver2.costoTotal() >solver.costoTotal());	
	}
	public void cargarconexiones4(ArrayList<DatosLocalidad> c){
		DatosLocalidad dato1 = new DatosLocalidad("Buenos Aires","Los Polvorines",53354,-34.5, -58.68);
		DatosLocalidad dato2 = new DatosLocalidad("Buenos Aires","pergamino",982,-33.89, -60.57);
        c.add(dato1);
        c.add(dato2);
	}
	public void cargarconexiones5(ArrayList<DatosLocalidad> c){
		DatosLocalidad dato1 = new DatosLocalidad("Buenos Aires","pergamino",982,-33.89, -60.57);
		DatosLocalidad dato2 = new DatosLocalidad("Santa F�","Venado Tuerto",13000,-33.74, -61.96);
	    c.add(dato1);
	    c.add(dato2);
	
	}
	public void cargarconexiones(ArrayList<DatosLocalidad> c){
		DatosLocalidad dato1 = new DatosLocalidad("Buenos Aires","Los Polvorines",53354,-34.5, -58.68);
		DatosLocalidad dato2 = new DatosLocalidad("Tucuman","Atahona",399,-27.41, -65.28);
		DatosLocalidad dato3 = new DatosLocalidad("Rio Negro","El Bols�n",13000,-41.96, -71.51);
		DatosLocalidad dato4 = new DatosLocalidad("Santa F�","Venado Tuerto",13000,-33.74, -61.96);
		c.add(dato1);
		c.add(dato2);
		c.add(dato3);
		c.add(dato4);
	}
	
	public void cargarconexiones2(ArrayList<DatosLocalidad> c){
		 
			DatosLocalidad dato1 = new DatosLocalidad("Buenos Aires","Los Polvorines",53354,-34.5, -58.68);
			DatosLocalidad dato2 = new DatosLocalidad("Tucuman","Atahona",399,-27.41, -65.28);
			DatosLocalidad dato3 = new DatosLocalidad("Rio Negro","El Bols�n",13000,-41.96, -71.51);
			DatosLocalidad dato4 = new DatosLocalidad("Santa F�","Venado Tuerto",13000,-33.74, -61.96);
			DatosLocalidad dato5 = new DatosLocalidad("Formosa","El Potrillo",13000,-23.5, -62.68);
			DatosLocalidad dato6 = new DatosLocalidad("Formosa","Buena Vista",887,-25.01, -58.43);
			DatosLocalidad dato7 = new DatosLocalidad("Chubut","Sarmiento",13000,-45.58, -69.06);
			DatosLocalidad dato8 = new DatosLocalidad("Chubut","Rawson",13000,-43.29, -65.09);
			DatosLocalidad dato9 = new DatosLocalidad("Buenos Aires","Villa Bourdeau",982,-38.7, -62.35);
			DatosLocalidad dato10 = new DatosLocalidad("Mendoza","Barrio Primavera",131,-34.63, -68.4);
			DatosLocalidad dato11 = new DatosLocalidad("Mendoza","Punta de Vacas",47,-32.85, -69.75);
			c.add(dato1);
			c.add(dato2);
			c.add(dato3);
			c.add(dato4);
			c.add(dato5);
			c.add(dato6);
			c.add(dato7);
			c.add(dato8);
			c.add(dato9);
			c.add(dato10);
			c.add(dato11);
	}

}
