package Tests;

import java.util.ArrayList;

import org.junit.Test;

import logica.AGM;
import logica.PesoArista;

public class Test_agm {

	@Test
	public void test() {
		
		ArrayList<PesoArista> PARISTA= new ArrayList<PesoArista> ();
		
		
		 PesoArista peso1= new PesoArista(0,1,4.0);
		 PesoArista peso2= new PesoArista(1,2,8.0);
		 PesoArista peso3= new PesoArista(2,3,6.0);
		 PesoArista peso4= new PesoArista(3,4,9.0);
		 PesoArista peso5= new PesoArista(4,5,10.0);
		 PesoArista peso6= new PesoArista(5,6,3.0);
		 PesoArista peso7= new PesoArista(6,7,1.0);
		 PesoArista peso8= new PesoArista(7,0,8.0);
		 PesoArista peso9= new PesoArista(7,8,6.0);
		 PesoArista peso10= new PesoArista(1,7,12.0);
		 PesoArista peso11= new PesoArista(2,8,3.0);
		 PesoArista peso12= new PesoArista(8,6,5.0);
		 PesoArista peso13= new PesoArista(2,5,4.0);
		 PesoArista peso14= new PesoArista(3,5,13.0);
	 
		 
		 PARISTA.add(peso1);
		 PARISTA.add(peso2);
		 PARISTA.add(peso3);
		 PARISTA.add(peso4);
		 PARISTA.add(peso5);
		 PARISTA.add(peso6);
		 PARISTA.add(peso7);
		 PARISTA.add(peso8);
		 PARISTA.add(peso9);
		 PARISTA.add(peso10);
		 PARISTA.add(peso11);
		 PARISTA.add(peso12);
		 PARISTA.add(peso13);
		 PARISTA.add(peso14);
		
	 		 
		 AGM arbol= new AGM(PARISTA, 9);
		 
		 
		arbol.arbolGM.agregarArista(0, 1);
		arbol.arbolGM.agregarArista(1, 2);
		arbol.arbolGM.agregarArista(2, 3);
		arbol.arbolGM.agregarArista(3, 4);
		arbol.arbolGM.agregarArista(4, 5);
		arbol.arbolGM.agregarArista(5, 6);
		arbol.arbolGM.agregarArista(6, 7);
		arbol.arbolGM.agregarArista(7, 0);
		arbol.arbolGM.agregarArista(7, 8);
		arbol.arbolGM.agregarArista(1, 7);
		arbol.arbolGM.agregarArista(2, 8);
		arbol.arbolGM.agregarArista(8, 6);
		arbol.arbolGM.agregarArista(2, 5);
		arbol.arbolGM.agregarArista(3, 5);
	
		
		
	System.out.println(arbol);
	System.out.println(arbol.aristasConPesos);
	
			
	
	
	}

}
