package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import logica.PesoArista;

public class Test_PesoArista {

	@Test
	public void test() {
		
		PesoArista dato1 = new PesoArista(1,0,23.0);
		PesoArista dato2 = new PesoArista(1,2,25.0);
		PesoArista dato3 = new PesoArista(2,3,26.0);
		PesoArista dato4 = new PesoArista(1,2,23.0);
		
		System.out.println("es igual el peso: "+ dato1.equals(dato4));
		System.out.println("dato 1 < dato 2  "+ dato1.compareTo(dato2));
		System.out.println("dato 1 = dato 4  "+ dato1.compareTo(dato4));
		System.out.println("dato 1 > dato 4  "+ dato3.compareTo(dato1));
	}

}
