package logica;


import java.util.ArrayList;


public class AGM {
	

	public ArrayList<PesoArista>  aristasConPesos;
	public Grafo arbolGM;
	int cantAristas;

	public AGM( ArrayList<PesoArista> pAristas, int cantVertices){
		aristasConPesos = pAristas;		
		cantAristas = 0;	
		arbolGM = new Grafo(cantVertices);	
	}
			
	public AGM  kruskal(){
		Double menorPeso;
		ArrayList<PesoArista> auxPesoarista= new ArrayList<PesoArista>(); 
		ArrayList<PesoArista> kruskalPesoArista= new ArrayList<PesoArista>();
		auxPesoarista.addAll(aristasConPesos);
		auxPesoarista.sort(null);
		int kruskalCantidadVertices=this.arbolGM.vertices();
		AGM kruskal_var = new AGM(auxPesoarista, kruskalCantidadVertices);
		int vertice1, vertice2;
	while (auxPesoarista.size()!=0){
		    menorPeso = auxPesoarista.get(0).getPeso();
			vertice1 = auxPesoarista.get(0).aristas.a;
			vertice2 = auxPesoarista.get(0).aristas.b;
			
			if(!kruskal_var.arbolGM.existeArista(vertice1, vertice2)){
				kruskal_var.arbolGM.agregarArista(vertice1,vertice2);
				PesoArista aux3= new PesoArista(vertice1,vertice2,menorPeso);
				kruskalPesoArista.add(aux3);
				 cantAristas ++;
		
			if (kruskal_var.arbolGM.grafoTieneCiclo()){
					kruskal_var.arbolGM.eliminarArista(vertice1,vertice2);
					cantAristas--;
					auxPesoarista.remove(0);
					kruskalPesoArista.remove(kruskalPesoArista.size()-1);
					kruskal_var.arbolGM.ciclos--;
			}
			else
					auxPesoarista.remove(0);
	
	}}
	kruskal_var.aristasConPesos.addAll(kruskalPesoArista);
	return kruskal_var;

	
}
    public AGM Prim(){
		
		ArrayList<PesoArista> auxPesoaristas= new ArrayList<PesoArista>(); 
		ArrayList<PesoArista> PrimPesoArista= new ArrayList<PesoArista>();
		auxPesoaristas.addAll(aristasConPesos);
		auxPesoaristas.sort(null);
		AGM prim_var = new AGM(auxPesoaristas, this.arbolGM.vertices());		
		int i=1;	
		
				while (i<prim_var.arbolGM.vertices()-1)
				{
					
					for( int j=0;j<auxPesoaristas.size();j++)
					{
						
							PesoArista aux2;

							    aux2 = new PesoArista(auxPesoaristas.get(j).aristas.a, auxPesoaristas.get(j).aristas.b, auxPesoaristas.get(j).peso);
                                PrimPesoArista.add(aux2);
           		                prim_var.arbolGM.agregarArista(aux2.aristas.a,aux2.aristas.b);
							
					       if ( prim_var.arbolGM.mismaComponenteConexa(PrimPesoArista.get(0).aristas.a,PrimPesoArista.size()) && prim_var.arbolGM.ciclos==0)
						    { 
		                        auxPesoaristas.remove(auxPesoaristas.get(j));
		                        j=-1;
		                      
		                        if(PrimPesoArista.size()-1==prim_var.arbolGM.vertices())
		                        {
		                        	prim_var.aristasConPesos.addAll(PrimPesoArista);
		                        	prim_var.cantAristas=PrimPesoArista.size();
		            				return prim_var;
		                        }
		                        	
						    }
					       else
					       {
					    	   if(prim_var.arbolGM.ciclos>0)
					    	   {
					    		   auxPesoaristas.remove(aux2);
					    		   prim_var.arbolGM.ciclos=0;
					    	   }
					   
					    	   PrimPesoArista.remove(aux2);
					   
          		                prim_var.arbolGM.eliminarArista(aux2.aristas.a,aux2.aristas.b);
					       }
				    }  
					i++;}
	
				prim_var.aristasConPesos.addAll(PrimPesoArista);
				return prim_var;
	       }
    @Override
	public String toString() {
		return "AGM [arbolGM=" + arbolGM + "]";}
		
	
	
}