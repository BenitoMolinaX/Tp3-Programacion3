package logica;

import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;
public class SolverPrim  {
	

		public ArrayList<DatosLocalidad> conexiones;
		public ArrayList<PesoArista>  aristasConPesos;
		public Costos costo;	
		public AGM arbolGM;
		int cantAristas;
	
		public SolverPrim(ArrayList<DatosLocalidad> c, Costos costo){
			this.conexiones = c;
			int cantVertices = c.size();	
			aristasConPesos = new ArrayList<PesoArista>();
			arbolGM = new AGM(aristasConPesos, cantVertices); 
			generarTodasLasAristasYpesosDelGrafo(costo);
		    arbolGM=arbolGM.Prim();
		    aristasConPesos=arbolGM.aristasConPesos;
		    cantAristas=aristasConPesos.size();
		}
		
		private void generarTodasLasAristasYpesosDelGrafo(Costos costo){
			PesoArista pesoArista;
		    Double peso;	
			for (int i = 0; i < conexiones.size(); i++){
				for(int j = i+1; j < conexiones.size(); j++){
					if(j!=i) {
						arbolGM.arbolGM.agregarArista(i,j);
						//pesoArista = new PesoArista(i,j,(calcularDistancia(i,j)));
						peso = calcularPeso(i,j, costo);
						//System.out.println(obtenerLocalidad(i)+"  " + obtenerLocalidad(j) + "  " + "$ " + peso);
						pesoArista = new PesoArista(i,j,peso);
						aristasConPesos.add(pesoArista);
						cantAristas++;
					}
				}
		   }
		}
		public Double calcularPeso(Integer arista1, Integer arista2, Costos costo){
			Double peso, costoXKM,costoDistintaProv,costoMasLimiteKM, limiteKM, KM;

			costoXKM = costo.getCostosDosCiudades();   
			costoDistintaProv = costo.getCostoEntreProvincias(); 
			costoMasLimiteKM = costo.getCostoMas200Km();   
			
			limiteKM = 200.0;
			KM = calcularDistancia(arista1,arista2);
			peso =  KM * costoXKM;
			if(obtenerProvincia(arista1) != obtenerProvincia(arista2)) peso = peso + costoDistintaProv;
			if(KM > limiteKM) peso = peso + costoMasLimiteKM;
					return peso;
			
		}
		public double calcularDistancia(Integer vertice1 ,Integer vertice2){
			Coordinate coordenada_i, coordenada_j;
			double distanciaKM;
			Coordinate coordVertice1 = obtenerCoordenada(vertice1);
			Coordinate coordVertice2=obtenerCoordenada(vertice2);
		   distanciaKM = distanciaCoord(coordVertice1.getLat(), coordVertice1.getLon(), coordVertice2.getLat(), coordVertice2.getLon());	
		   return distanciaKM;
		}
		public  Coordinate obtenerCoordenada(Integer vertice){
			Coordinate coordenada = new Coordinate(conexiones.get(vertice).latitud, conexiones.get(vertice).longitud);
			return coordenada;
		}		
		public String obtenerLocalidad(int pos){
			String localidad = conexiones.get(pos).Localidad;
			return localidad;
		}	
		public String obtenerProvincia(int pos){
			String provincia = conexiones.get(pos).nombreProvincia;
			return provincia;
		}	
		private double distanciaCoord(double lat1, double lng1, double lat2, double lng2) {
		//double radioTierra = 3958.75;//en millas  
		double radioTierra = 6371;//en kilómetros  
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1); 
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2); 
		double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)* Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)); 
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
		double distancia = radioTierra * va2;
		return distancia;
		}		
		public double costoTotal(){
			double costoT;
			costoT = 0;
			for(int i = 0 ; i < aristasConPesos.size(); i ++)
				costoT = costoT + aristasConPesos.get(i).getPeso();
			return costoT;
		}
		
		
	}


