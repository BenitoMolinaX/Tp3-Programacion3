package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import logica.AGM;
import logica.Costos;
import logica.DatosLocalidad;
import logica.Solver;
import logica.Validaciones;
import xml.GuardarXML;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import java.awt.GridLayout;
import java.awt.CardLayout;
import javax.swing.JPanel;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Point;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.FlowLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.lang.model.element.Element;
import javax.naming.Context;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.awt.Font;
import javax.swing.ImageIcon;


public class Interfaz {

	private JFrame frame_interfaz;
	private Dimension tamanoMapa;
	private JMapViewer mapa;
	private JTable table;
	private JTable grid_conexiones;
	private JTextField provincia;
	private JTextField localidad;
	private JTextField habitantes;
	private JTextField latitud;
	private JTextField longitud;
	private ArrayList<DatosLocalidad> conexiones;
	
	DefaultTableModel modelo;
	Coordinate coord;
	MapMarker mark;
	
	private String constant;
	
	private Costos costo;
	
	private ArrayList<String > resultado = new ArrayList<String>();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.frame_interfaz.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {
		
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		conexiones = new ArrayList<DatosLocalidad> ();
		costo = new Costos();
		setCostosDefault(1500.00, 5000.00,1000.00);
		
		tamanoMapa = new Dimension(1000,1000);
		
		frame_interfaz = new JFrame();
		frame_interfaz.setBounds(0, 0, 1000, 718);
		frame_interfaz.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_interfaz.getContentPane().setLayout(null);
		
		mapa = new JMapViewer();
		mapa.setZoom(4);
		mapa.setLocation(16, 5);
		mapa.setBorder(new LineBorder(Color.BLUE));
	
		JPanel panel = new JPanel();
		panel.setBounds(10, 53, 423, 627);
	
		frame_interfaz.getContentPane().add(panel);
		panel.setLayout(null);
		panel.add(mapa);
		
		mapa.setSize(new Dimension(402, 611));
		mapa.setDisplayPositionByLatLon( -38, -64, 4);		
		grid_conexiones = new JTable();
		grid_conexiones.setEnabled(false);
		grid_conexiones.setBorder(new LineBorder(Color.BLUE));
		grid_conexiones.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Provincia", "Localidad", "Cant. Hab.", "Latitud", "Longitud"
			}
		));
		grid_conexiones.setBounds(450, 334, 475, 287);
		frame_interfaz.getContentPane().add(grid_conexiones);
		
		JLabel lblProvincia = new JLabel("Cant. Habitantes");
		lblProvincia.setBounds(655, 196, 98, 14);
		frame_interfaz.getContentPane().add(lblProvincia);
		
		JLabel label = new JLabel("Provincia");
		label.setBounds(448, 196, 98, 14);
		frame_interfaz.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("Localidad");
		label_1.setBounds(556, 196, 67, 14);
		frame_interfaz.getContentPane().add(label_1);
		
		JLabel lblLatitud = new JLabel("Latitud");
		lblLatitud.setBounds(766, 196, 67, 14);
		frame_interfaz.getContentPane().add(lblLatitud);
		
		JLabel lblLongitud = new JLabel("Longitud");
		lblLongitud.setBounds(870, 196, 75, 14);
		frame_interfaz.getContentPane().add(lblLongitud);
		
		provincia = new JTextField();
		provincia.setBounds(448, 217, 98, 20);
		frame_interfaz.getContentPane().add(provincia);
		provincia.setColumns(10);
		
		localidad = new JTextField();
		localidad.setBounds(551, 217, 98, 20);
		frame_interfaz.getContentPane().add(localidad);
		localidad.setColumns(10);
		
		habitantes = new JTextField();
		habitantes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				Validaciones val = new Validaciones();
				val.validarNumeros(arg0);
			}
		});
		habitantes.setBounds(656, 217, 98, 20);
		frame_interfaz.getContentPane().add(habitantes);
		habitantes.setColumns(10);
		
		latitud = new JTextField();
		latitud.addKeyListener(new KeyAdapter() {		
			@Override
			public void keyTyped(KeyEvent e) {
				Validaciones val = new Validaciones();
				val.validarLongLat(e);
			}
		});
		latitud.setBounds(764, 217, 98, 20);
		frame_interfaz.getContentPane().add(latitud);
		latitud.setColumns(10);
		
		longitud = new JTextField();
		longitud.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				Validaciones val = new Validaciones();
				val.validarLongLat(e);
			}
		});
		longitud.setBounds(870, 217, 98, 20);
		frame_interfaz.getContentPane().add(longitud);
		longitud.setColumns(10);
		
		
		modelo = (DefaultTableModel) grid_conexiones.getModel();   //modelo para la grilla
		
		
				
		JButton btnAgregarLocalidad = new JButton("Agregar Conexi\u00F3n");
		btnAgregarLocalidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DatosLocalidad nuevaConexion;
				String prov,loc;
				Integer hab;
				Double lat,lon;
				
				boolean validados = true; 
				
				validados = validados && validarcampos(provincia) && validarcampos(localidad) && validarcampos(habitantes) && validarcampos(latitud) && validarcampos(longitud);
				
				if(validados == true ){
					prov = provincia.getText();
					loc = localidad.getText();
				    hab = Integer.parseInt(habitantes.getText());
					lat = Double.parseDouble(latitud.getText());
					lon = Double.parseDouble(longitud.getText());
			 
					agregarLocalidadMapa(lat,lon,loc);
					nuevaConexion = new DatosLocalidad(prov,loc,hab,lat,lon);
					
					conexiones.add(nuevaConexion);
					
					modelo.addRow(new Object[]{ provincia.getText(), localidad.getText(), 
							habitantes.getText(), latitud.getText(),longitud.getText()
							});
					provincia.setText("");
					localidad.setText("");
					habitantes.setText("");
					latitud.setText("");
					longitud.setText("");
				}else{
					 JOptionPane.showMessageDialog(null,"Complete todos los campos");
				}
								
			}

			private boolean validarcampos(JTextField texto) {
				Validaciones val = new Validaciones();
				return  val.noEsTextoVacio(texto);
			
			}
			
			
		});
		btnAgregarLocalidad.setBounds(448, 271, 153, 32);
		frame_interfaz.getContentPane().add(btnAgregarLocalidad);
		
		
		JLabel lblNewLabel = new JLabel("La planificaci\u00F3n se realizar\u00E1 en base a los siguientes par\u00E1metros:");
		lblNewLabel.setBounds(448, 53, 487, 14);
		frame_interfaz.getContentPane().add(lblNewLabel);
		
		JLabel lblCostoPorKilmetro = new JLabel("Costo por kil\u00F3metro entre dos ciudades:");
		lblCostoPorKilmetro.setBounds(448, 74, 230, 14);
		frame_interfaz.getContentPane().add(lblCostoPorKilmetro);
		
		JLabel lblCostoEntreProvincias = new JLabel("Costo agregado entre provincias distintas:");
		lblCostoEntreProvincias.setBounds(448, 90, 250, 14);
		frame_interfaz.getContentPane().add(lblCostoEntreProvincias);
		
		JLabel lblCostoPorConexin = new JLabel("Costo agregado por conexi\u00F3n de m\u00E1s de 200 km. :");
		lblCostoPorConexin.setBounds(448, 106, 295, 14);
		frame_interfaz.getContentPane().add(lblCostoPorConexin);
		
		//BOTON PLANIFICACION �PTIMA
		
		JButton btn_ObtenerPlan = new JButton("Obtener Planificaci\u00F3n Kruskal");
		btn_ObtenerPlan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(conexiones.size()>0){
				constant = "1";
				mapa.removeAllMapMarkers();
				resultado.clear();
				Solver s = new Solver(conexiones, costo);
										
					AGM arbolKruskal  = s.arbolGM.kruskal();
					dibujarAGMenMapa(arbolKruskal, s);
					System.out.println("conexiones :"+ s.conexiones);
					System.out.println("Costo Total =  $ " + s.costoTotal());
					DecimalFormat f = new DecimalFormat("##.00");
//					JOptionPane.showMessageDialog(null, "Costo: "+ f.format(s.costoTotal()));
					Resultado jp = new Resultado(resultado, f.format(s.costoTotal()) );
					jp.setVisible(true);
					btn_ObtenerPlan.setEnabled(false);
				}
				else{
					JOptionPane.showMessageDialog(null,"Cargue los datos de conexiones");
				}
			}
		});
		btn_ObtenerPlan.setBounds(448, 640, 201, 32);
		frame_interfaz.getContentPane().add(btn_ObtenerPlan);
		
//		Muestro los costos en los lables. Y defino al evento del mouseclick
		
		String aux = Double.toString(costo.getCostosDosCiudades());
		JLabel lbl_costokm = new JLabel(aux);
		lbl_costokm.setName("lblCostoPorKilmetro");
		lbl_costokm.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println(lbl_costokm.getName());
				SetPrecios(lbl_costokm);
			}
		});
		lbl_costokm.setForeground(Color.BLUE);
		lbl_costokm.setBounds(760, 74, 70, 14);
		frame_interfaz.getContentPane().add(lbl_costokm);
		
		aux = Double.toString(costo.getCostoMas200Km());
		JLabel lbl_costoagregado = new JLabel(aux);
		lbl_costoagregado.setName("lbl_costoagregado");
		lbl_costoagregado.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				SetPrecios(lbl_costoagregado);
			}
		});
		lbl_costoagregado.setForeground(Color.BLUE);
		lbl_costoagregado.setBounds(760, 90, 70, 14);
		frame_interfaz.getContentPane().add(lbl_costoagregado);
		
		aux = Double.toString(costo.getCostoEntreProvincias());
		JLabel lbl_costoprovincias = new JLabel(aux);
		lbl_costoprovincias.setName("lbl_costoprovincias");
		lbl_costoprovincias.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				SetPrecios(lbl_costoprovincias);
			}
		});
		lbl_costoprovincias.setForeground(Color.BLUE);
		lbl_costoprovincias.setBounds(760, 106, 70, 14);
		frame_interfaz.getContentPane().add(lbl_costoprovincias);
		
		JLabel lblConexionesAgregadas = new JLabel("Conexiones Agregadas");
		lblConexionesAgregadas.setBounds(448, 314, 190, 14);
		frame_interfaz.getContentPane().add(lblConexionesAgregadas);
		
		JButton btn_eliminarfila = new JButton("Deshacer Ultimo");
		btn_eliminarfila.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(grid_conexiones.getRowCount()>=1){
					conexiones.remove(conexiones.size()-1);
					modelo.removeRow(grid_conexiones.getRowCount()-1);
				}else{
					JOptionPane.showMessageDialog(null,"No se puede eliminar");
				}
			}
		});
		btn_eliminarfila.setBounds(625, 271, 153, 32);
		frame_interfaz.getContentPane().add(btn_eliminarfila);
		
		JButton btn_verMapa = new JButton("Ver en Mapa");
		btn_verMapa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String loc;
				if(validar_latitudlongitud()){
					mapa.removeAllMapMarkers();
					loc = localidad.getText();
					mark = new MapMarkerDot(loc, coord);
					mapa.addMapMarker(mark);
				}
				
			}

	
		});
		btn_verMapa.setBounds(843, 248, 107, 23);
		frame_interfaz.getContentPane().add(btn_verMapa);
		
		JLabel lbl_configurar = new JLabel("* Para modificar los costos haga click sobre el valor.");
		lbl_configurar.setFont(new Font("Tahoma", Font.BOLD, 10));
		lbl_configurar.setBounds(458, 131, 487, 14);
		frame_interfaz.getContentPane().add(lbl_configurar);
		
		JButton btnPanificacinNoOptima = new JButton("Obtener Panificaci\u00F3n Prim");
		btnPanificacinNoOptima.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DecimalFormat f = new DecimalFormat("##.00");
				if(conexiones.size()>0 ){
				constant = "2";
				resultado.clear();
				Solver solver = new Solver(conexiones,  costo); 
						
				AGM arbolPrim = solver.arbolGM.Prim();
				dibujarAGMenMapa(arbolPrim, solver);
					        
//				JOptionPane.showMessageDialog(null, "No optimo: "+ f.format(solver.costoTotal()));
				
//				System.out.println("No optima " + solver.costoTotal());
				Resultado jp = new Resultado(resultado, f.format(solver.costoTotal()) );
				jp.setVisible(true);
				btnPanificacinNoOptima.setEnabled(false);
				}else{
					JOptionPane.showMessageDialog(null,"Cargue los datos de conexiones");
				}
			}
		});
		btnPanificacinNoOptima.setBounds(661, 640, 190, 32);
		frame_interfaz.getContentPane().add(btnPanificacinNoOptima);
		
		JButton btn_loadfile = new JButton("");
		btn_loadfile.setIcon(new ImageIcon(Interfaz.class.getResource("/javax/swing/plaf/metal/icons/ocean/file.gif")));
		btn_loadfile.setToolTipText("Abrir archivo");
		btn_loadfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DatosLocalidad nuevaConexion;
				String prov,loc;
				Integer hab;
				Double lat,lon;
				
				
				 File file = new File("");
				 
				 file = abrirarchivo();
				if(file != null){
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
				        .newInstance();
				DocumentBuilder documentBuilder = null;
				try {
					documentBuilder = documentBuilderFactory.newDocumentBuilder();
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				org.w3c.dom.Document document = null;
				try {
					document =  documentBuilder.parse(file);
				} catch (SAXException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				NodeList nodeList = document.getElementsByTagName("dato");
				
			    for (int i = 0; i < nodeList.getLength(); i++) {
			    	
			        Node node = nodeList.item(i);
			        if (node.getNodeType() == Node.ELEMENT_NODE) {

			        	String habs,lats,lons;
			        	prov = ((org.w3c.dom.Document) document).getElementsByTagName("provincia").item(i).getTextContent();
			        	loc = ((org.w3c.dom.Document) document).getElementsByTagName("localidad").item(i).getTextContent();
			        	habs = ((org.w3c.dom.Document) document).getElementsByTagName("habitantes").item(i).getTextContent();
			        	hab = Integer.parseInt(habs);
			        	lats = ((org.w3c.dom.Document) document).getElementsByTagName("lat").item(i).getTextContent();
			        	lat = Double.parseDouble(lats);
			        	lons = ((org.w3c.dom.Document) document).getElementsByTagName("long").item(i).getTextContent();
			        	lon = Double.parseDouble(lons);
			        	nuevaConexion = new DatosLocalidad(prov,loc,hab,lat,lon);
			        	conexiones.add(nuevaConexion);
//					Agrego a la grilla de conexiones
			        	modelo.addRow(new Object[]{ prov, loc, 
								habs, lats,lons
								});
						
			        }
			    }
				}
				    
			}

			
		});
		btn_loadfile.setBounds(935, 330, 43, 32);
		frame_interfaz.getContentPane().add(btn_loadfile);
		
		JButton btn_borrar = new JButton("");
		btn_borrar.setToolTipText("resetear mapa");
		btn_borrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnPanificacinNoOptima.setEnabled(true);
				btn_ObtenerPlan.setEnabled(true);
				mapa.removeAll();
				mapa.removeAllMapPolygons();
				mapa.removeAllMapMarkers();
				conexiones.clear();
				resultado.clear();
				int r = modelo.getRowCount()-1;
				while(modelo.getRowCount()>0){
//					System.out.println( " r "+r);
					modelo.removeRow(r);
					r--;
				}
				
			}
		});
		btn_borrar.setIcon(new ImageIcon(Interfaz.class.getResource("/com/sun/javafx/webkit/prism/resources/missingImage.png")));
		btn_borrar.setBounds(935, 369, 43, 32);
		frame_interfaz.getContentPane().add(btn_borrar);
		
		JButton btn_save = new JButton("");
		btn_save.setToolTipText("Guarda ultima planificacion");
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(conexiones.size()>0){
				String nombre_archivo = "coordenadas";
				 String directorio  = "";
				File file = new File("");
				 			 
				JFileChooser f =new JFileChooser();
				int r = f.showSaveDialog(f);
				if(r == JFileChooser.APPROVE_OPTION){
				nombre_archivo = f.getSelectedFile().getName();
				 directorio = f.getCurrentDirectory().toString();
//				 nombre_archivo = directorio.concat("\\"+nombre_archivo);
				 System.out.println("nombre archivo "+ nombre_archivo);
//				 File file = new File(nombre_archivo);
				}else{
					
				}
				if(file != null){			
				System.out.println(nombre_archivo);
				GuardarXML save = new GuardarXML(conexiones, nombre_archivo, directorio);
				
				}
				}else{
					JOptionPane.showMessageDialog(null,"No hay conexiones cargadas");
				}
			
			}
		});
		btn_save.setIcon(new ImageIcon(Interfaz.class.getResource("/javax/swing/plaf/metal/icons/ocean/floppy.gif")));
		btn_save.setBounds(935, 412, 43, 32);
		frame_interfaz.getContentPane().add(btn_save);

		
	}
		private File abrirarchivo() {
			File file = null;
			JFileChooser f =new JFileChooser();
			int r = f.showOpenDialog(f);
			if(r == JFileChooser.APPROVE_OPTION){
			 File abre=f.getSelectedFile();
			 file = new File(abre.toString());
			}else{
				
			}
			  
			return file;
		}
		private void guardarArchivo() {
			String nombre_archivo;
			 
			JFileChooser f =new JFileChooser();
			int r = f.showSaveDialog(f);
			if(r == JFileChooser.APPROVE_OPTION){
			 File abre=f.getSelectedFile();
			 nombre_archivo = f.getName();
//			 f.getCfileurrentDirectory();
			 System.out.println(nombre_archivo);
			 File file = new File(nombre_archivo);
			}else{
				
			}
			  
			
		}
	public void agregarLocalidadMapa(Double latitud, Double longitud, String localidad){
		Coordinate coordenada = new Coordinate(latitud,longitud);
		MapMarker marcador = new MapMarkerDot(localidad, coordenada);
		mapa.addMapMarker(marcador);
		
    }
	
	public void dibujarArista(ArrayList<Coordinate> coordenadas){
		MapPolygon polygon = new MapPolygonImpl(coordenadas);
		mapa.addMapPolygon(polygon);
	}
	
	private boolean validar_latitudlongitud() {
		Double lat = 0.0;
		Double lon = 0.0; 
		
		try{
			lat = Double.parseDouble(latitud.getText());
			lon = Double.parseDouble(longitud.getText());
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null,"Ingrese un numero valido");
		}
		
		coord = new Coordinate(lat,lon);
		return true;
	}
	
	public void setCostosDefault (Double c, Double m, Double p){
		
		this.costo.setCostoDosCiudades(c);
		this.costo.setCostoMas200Km(m);
		this.costo.setCostoEntreProvincias(p);
				
	}
	
	public Costos getCostos (){
		return this.costo;
	}
	
	private void SetPrecios(JLabel lbl) {
		boolean esOk = false;
		Double auxd = 0.0;
		String TextoIngreso = "";
		String label = lbl.getName();
		
		if(label == "lblCostoPorKilmetro"){
			auxd = costo.getCostosDosCiudades();
			TextoIngreso = "Ingreso Costo entre dos Ciudades";
		}
		if(label == "lbl_costoagregado"){
			auxd = costo.getCostoMas200Km();
			TextoIngreso = "Ingreso Costo por mas de 200 km";
		}
		if(label == "lbl_costoprovincias"){
			auxd = costo.getCostoEntreProvincias();
			TextoIngreso = "Ingreso Costo por conectar 2 provincias";
		}
		
				
		String aux = JOptionPane.showInputDialog("costo por cambio prov", Double.toString(auxd));
		if(aux != null){
		 try{
			 auxd = Double.parseDouble(aux);
			 switch (label) {
			 case "lblCostoPorKilmetro" :
				 costo.setCostoDosCiudades(auxd);
				 break;
			 case "lbl_costoagregado" :
				 costo.setCostoMas200Km(auxd);
				 break;
			 case "lblCostoEntreProvincias" :
				 costo.setCostoEntreProvincias(auxd);
			  break;
			 }
			 lbl.setText(aux);
			 esOk = true;
		 }
		 catch(NumberFormatException arg0){
			  JOptionPane.showMessageDialog(lbl, "Error de formato", "ERROR", JOptionPane.ERROR_MESSAGE);
			  esOk = false;
		 }
		}
	
	}

	
	
	// DIBUJAR MAPA
	
	public void dibujarAGMenMapa(AGM arbol, Solver solver){
		Coordinate coordV1,coordV2;
		double peso;

		          for(Integer j = 0 ; j < arbol.aristasConPesos.size() ; j ++){
				    	int v1 = arbol.aristasConPesos.get(j).getArista().getx();
				        int v2 = arbol.aristasConPesos.get(j).getArista().gety();
				        coordV1 = solver.obtenerCoordenada(v1);
				        coordV2 = solver.obtenerCoordenada(v2);
				        peso = Math.round(arbol.aristasConPesos.get(j).getPeso());
				        dibujarArista(coordV1, coordV2);
				        
						mark = new MapMarkerDot(solver.obtenerLocalidad(v1), coordV1);
						mapa.addMapMarker(mark);
						mark = new MapMarkerDot(solver.obtenerLocalidad(v2), coordV2);
						mapa.addMapMarker(mark);
				        resultado.add(solver.obtenerLocalidad(v1).toString() + " con "+ solver.obtenerLocalidad(v2) + v1 + "---" + v2 + "  Peso:   " + peso);
				        
				        
//				        System.out.println(solver.obtenerLocalidad(v1) + " con "+ solver.obtenerLocalidad(v2) + v1 + "---" + v2 + "  Peso:   " + peso); 
		           }
	}
	
	
	public void dibujarArista(Coordinate v1, Coordinate v2){
		ArrayList<Coordinate> coordenadas = new ArrayList<Coordinate>();
		coordenadas.add(v1);
		coordenadas.add(v2);
		coordenadas.add(v1);
		dibujar(coordenadas);
	}
	 private void dibujar(ArrayList<Coordinate> c){
		 Color  colorAristas = new Color(0, 204,0);
		 if(constant == "2"){
			 colorAristas = new Color(204, 0,0);
		 }
	  MapPolygonImpl pol = new MapPolygonImpl(c);
		pol.setColor(colorAristas);
		 pol.setBackColor(colorAristas);
		pol.setStroke(new BasicStroke(2));
		mapa.addMapPolygon(pol);
	}
}
