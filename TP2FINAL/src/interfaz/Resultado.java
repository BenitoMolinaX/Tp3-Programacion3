package interfaz;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

public class Resultado extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */


	/**
	 * Create the dialog.
	 */
	public Resultado(ArrayList<String> resultado, String costo) {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lbl_costo = new JLabel("");
		lbl_costo.setBounds(24, 37, 400, 20);
		contentPanel.add(lbl_costo);
		lbl_costo.setText("Costo total: " +costo);
		
		String aux = null;
		
		for (int i=0;i<resultado.size();i++){
			JLabel lbl_result = new JLabel("resultado:");
			
			lbl_result.setBounds(10, i*10, 414, 230);
			lbl_result.setText(resultado.get(i));
			contentPanel.add(lbl_result);
		}
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
		}
	}
}
